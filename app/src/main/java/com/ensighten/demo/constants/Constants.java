package com.ensighten.demo.constants;

/**
 * Class containing a list of constants used by the app.
 */
public class Constants {

    /**
     * Android Resource Types
     */
    public static final String RESOURCE_DRAWABLE = "drawable";

    /**
     * Cart
     */
    public static final String CART_ITEMS_FILE = "cartItems";
    public static final String CART_ITEMS_KEY = "cartItems";

    /**
     * Common
     */
    public static final int INVALID_RESOURCE_ID = 0;
    public static final String EMPTY_STRING = "";
    public static final String ZERO = "0";

    /**
     * Conversions
     */
    public static final String PROMOTION_ADDED_TO_CART_CONVERSION = "Promotion Added to Cart";

    /**
     * Custom Methods
     */
    public static final String CHECKOUT_CUSTOM_METHOD = "trackCheckout";

    /**
     * Debug
     */
    public static final String LOGCAT_TAG = "EnsightenDemo";

    /**
     * Events
     */
    public static final String ADD_TO_CART_EVENT = "Add to Cart";
    public static final String VIEW_CART_EVENT = "View Cart";
    public static final String ITEM_QUANTITY_CHANGED_EVENT = "Item Quantity Changed";
    public static final String ITEM_REMOVED_EVENT = "Item Removed Event";

    /**
     * JSON
     */
    public static final String JSON_PROMOTIONS = "promotions";
    public static final String JSON_PRODUCTS = "products";
    public static final String JSON_ITEMS = "items";
    public static final String JSON_PRODUCT_ID = "productId";
    public static final String JSON_PRODUCT_NAME = "productName";
    public static final String JSON_PRODUCT_PRICE = "productPrice";
    public static final String JSON_PROMOTION_ID = "promotionId";
    public static final String JSON_PROMOTION_NAME = "promotionName";
    public static final String JSON_PROMOTION_PRICE = "promotionPrice";
    public static final String JSON_ITEM_ID = "itemId";
    public static final String JSON_ITEM_NAME = "itemName";
    public static final String JSON_ITEM_PRICE = "itemPrice";
    public static final String JSON_ITEM_QUANTITY = "itemQuantity";

    /**
     * Page Names
     */
    public static final String HOME_PAGE = "Home Page";
    public static final String PRODUCT_DETAIL_PAGE = "Product Detail Page";
    public static final String PROMOTION_DETAIL_PAGE = "Promotion Page";
    public static final String CART_PAGE = "Cart Page";
    public static final String CHECKOUT_COMPLETE_PAGE = "Checkout Complete Page";

    /**
     * Privacy
     */
    public static final String KEY_PRIVACY_TRANSLATION = "privacyTranslation";
    public static final String KEY_PRIVACY_COOKIES = "privacyCookies";
    public static final String PRIVACY_MODAL_TAG = "ensightenPrivacyCustomModal";
    public static final int PRIVACY_CUSTOM_DIALOG_BACKGROUND_COLOR_FILTER_MASK = 0xFF000000;

    /**
     * Tags
     */
    public static final String TAG_PRODUCT_ID = "PRODUCT_ID";

    /**
     * XML Tags and Attributes
     */
    public static final String XML_TAG_STORE = "Store";
    public static final String XML_TAG_PRODUCTS = "Products";
    public static final String XML_TAG_PRODUCT = "Product";
    public static final String XML_TAG_PROMOTIONS = "Promotions";
    public static final String XML_TAG_PROMOTION = "Promotion";
    public static final String XML_ATTRIBUTE_ID = "id";
    public static final String XML_ATTRIBUTE_NAME = "name";
    public static final String XML_ATTRIBUTE_DESCRIPTION = "description";
    public static final String XML_ATTRIBUTE_IMAGE = "image";
    public static final String XML_ATTRIBUTE_THUMBNAIL = "thumbnail";
    public static final String XML_ATTRIBUTE_LARGE_IMAGE = "largeImage";
    public static final String XML_ATTRIBUTE_PRICE = "price";

}
