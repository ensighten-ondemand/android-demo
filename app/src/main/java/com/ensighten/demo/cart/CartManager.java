package com.ensighten.demo.cart;

import android.content.Context;
import android.content.SharedPreferences;

import com.ensighten.Ensighten;
import com.ensighten.demo.constants.Constants;
import com.ensighten.demo.model.CartItem;
import com.ensighten.demo.model.Item;
import com.ensighten.demo.tracker.DemoTracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Cart manager instance that contains the items in the shopping cart.
 */
public class CartManager {

    private static CartManager sInstance;

    public static CartManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new CartManager(context.getApplicationContext());
        }
        return sInstance;
    }

    /**
     * The context of the application.
     */
    private Context mContext;

    /**
     * The list of items in the shopping cart.
     */
    private List<CartItem> mItems;

    /**
     * The shared preferences used for saving the cart.
     */
    private final SharedPreferences mPreferences;

    /**
     * The GSON instance used for saving items to the cart.
     */
    private final Gson mGson;

    /**
     * Create the cart and instantiate the cart.
     *
     * @param context the context of the application.
     */
    private CartManager(Context context) {
        mContext = context;
        mItems = new ArrayList<>();
        mPreferences = context.getSharedPreferences(Constants.CART_ITEMS_FILE, Context.MODE_PRIVATE);
        mGson = new Gson();
        loadCart();
    }

    /**
     * Cleanup the list by removing all items with quantity less than 1.
     */
    public void addToCart(Item itemToAdd) {
        if (itemToAdd == null)
            return;

        for (CartItem item : mItems) {
            if (item.getId().equals(itemToAdd.getId())) {
                item.plusOne();
                saveCart();
                return;
            }
        }

        mItems.add(new CartItem(itemToAdd.getId(), itemToAdd.getName(), itemToAdd.getThumbnail(), itemToAdd.getPrice()));
        HashMap<String, String> data = new HashMap<>();
        data.put(Constants.JSON_ITEM_ID, itemToAdd.getId());
        data.put(Constants.JSON_ITEM_NAME, itemToAdd.getName());
        data.put(Constants.JSON_ITEM_PRICE, NumberFormat.getCurrencyInstance().format(itemToAdd.getPrice()));
        Ensighten.trackEvent(Constants.ADD_TO_CART_EVENT, data);
        saveCart();
    }

    /**
     * Cleanup the list by removing all items with quantity less than 1.
     */
    public void cleanupItems() {
        for (CartItem item : new ArrayList<>(mItems)) {
            if (item.getQuantity() < 1) {
                mItems.remove(item);
                HashMap<String, String> data = new HashMap<String, String>();
                data.put(Constants.JSON_ITEM_ID, item.getId());
                data.put(Constants.JSON_ITEM_NAME, item.getName());
                data.put(Constants.JSON_ITEM_PRICE, NumberFormat.getCurrencyInstance().format(item.getPrice()));
                Ensighten.trackEvent(Constants.ITEM_REMOVED_EVENT, data);
            }
        }
        saveCart();
    }

    /**
     * Return true if the cart is empty.
     *
     * @return true if the cart is empty.
     */
    public boolean isCartEmpty() {
        return mItems.isEmpty();
    }

    /**
     * Checkout the cart. Empty the cart and save the cart to disk.
     */
    public void checkout() {
        ((DemoTracker) Ensighten.getTracker()).trackCheckout(NumberFormat.getCurrencyInstance().format(getCartTotal()), mItems.size(), mItems);
        mItems.clear();
        saveCart();
    }

    /**
     * Get the total price of all the items in the cart.
     */
    public float getCartTotal() {
        float cartTotal = 0;
        for (CartItem cartItem : mItems) {
            cartTotal += cartItem.getTotalPrice();
        }
        return cartTotal;
    }

    /**
     * Save the cart to preferences.
     */
    public void saveCart() {
        mPreferences.edit().putString(Constants.CART_ITEMS_KEY, mGson.toJson(mItems)).commit();
    }

    /**
     * Load the cart from preferences.
     */
    public void loadCart() {
        Type type = new TypeToken < List < CartItem >> () {}.getType();
        List<CartItem> cartItems = mGson.fromJson(mPreferences.getString(Constants.CART_ITEMS_KEY, Constants.EMPTY_STRING), type);
        if (cartItems != null) {
            mItems = cartItems;
        }
    }

    public List<CartItem> getItems() {
        return mItems;
    }

}

