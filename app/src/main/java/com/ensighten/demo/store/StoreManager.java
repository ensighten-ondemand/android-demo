package com.ensighten.demo.store;

import android.content.Context;
import android.util.Log;

import com.ensighten.demo.R;
import com.ensighten.demo.debug.Debug;
import com.ensighten.demo.model.Store;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * StoreManager instance that contains the list of products and promotions that are available in the store.
 */
public class StoreManager {

    private static StoreManager sInstance;

    public static StoreManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new StoreManager(context.getApplicationContext());
        }
        return sInstance;
    }

    /**
     * The context of the application.
     */
    private Context mContext;

    /**
     * The store containing the list of products and promotions.
     */
    private Store mStore;

    /**
     * Create the store and populate the it using products from the store.xml file.
     *
     * @param context the context of the application.
     */
    private StoreManager(Context context) {
        mContext = context;
        XmlPullParser parser = context.getResources().getXml(R.xml.store);
        try {
            parser.next();
            parser.next();
            mStore = new StoreXmlParser().readStore(context, parser);
        } catch (XmlPullParserException e) {
            Debug.printError(Debug.StoreManagerMessages.ERROR_UNABLE_TO_READ_STORE, e);
        } catch (IOException e) {
            Debug.printError(Debug.StoreManagerMessages.ERROR_UNABLE_TO_READ_STORE, e);
        }
    }

    public Store getStore() {
        return mStore;
    }

    public void setStore(Store store) {
        this.mStore = store;
    }

}
