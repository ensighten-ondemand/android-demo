package com.ensighten.demo.store;

import android.content.Context;
import android.util.Xml;

import com.ensighten.demo.constants.Constants;
import com.ensighten.demo.model.Product;
import com.ensighten.demo.model.Promotion;
import com.ensighten.demo.model.Store;
import com.ensighten.demo.utilities.Utilities;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used for parsing an xml file with products and promotions.
 */
public class StoreXmlParser {

    /**
     * The list of products offered at the store.
     */
    private List<Product> mProducts;

    public Store parseStore(Context context, InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readStore(context, parser);
        } finally {
            in.close();
        }
    }

    /**
     * Read the store data using the specified XML pull parser.
     *
     * @param parser the XML parser containing the store data to read.
     */
    public Store readStore(Context context, XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, null, Constants.XML_TAG_STORE);
        List<Product> products = new ArrayList<Product>();
        List<Promotion> promotions = new ArrayList<Promotion>();
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals(Constants.XML_TAG_PRODUCTS)) {
                products = readProducts(context, parser);
            } else if (name.equals(Constants.XML_TAG_PROMOTIONS)) {
                promotions = readPromotions(context, parser);
            } else {
                skip(parser);
            }
        }
        return new Store(products, promotions);
    }

    /**
     * Read the list of products using the specified XML pull parser.
     *
     * @param parser the XML parser containing the products to read.
     */
    private List<Product> readProducts(Context context, XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, null, Constants.XML_TAG_PRODUCTS);
        List<Product> products = new ArrayList<Product>();
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals(Constants.XML_TAG_PRODUCT)) {
                Product product = readProduct(context, parser);
                if (product != null)
                    products.add(product);
            } else {
                skip(parser);
            }
        }
        return products;
    }

    /**
     * Read a product using the specified XML pull parser.
     *
     * @param parser the XML parser containing the product to read.
     */
    private Product readProduct(Context context, XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, null, Constants.XML_TAG_PRODUCT);
        String id = parser.getAttributeValue(null, Constants.XML_ATTRIBUTE_ID);
        String name = parser.getAttributeValue(null, Constants.XML_ATTRIBUTE_NAME);
        String description = parser.getAttributeValue(null, Constants.XML_ATTRIBUTE_DESCRIPTION);
        String thumbnail = parser.getAttributeValue(null, Constants.XML_ATTRIBUTE_THUMBNAIL);
        List<String> images = new ArrayList<String>();
        images.add(thumbnail);
        int index = 1;
        while ((Utilities.getImageIdByName(context, thumbnail + index)) != Constants.INVALID_RESOURCE_ID) {
            images.add(thumbnail + index++);
        }
        float price = Float.parseFloat(parser.getAttributeValue(null, Constants.XML_ATTRIBUTE_PRICE));
        Product product = new Product(id, name, description, thumbnail, images, price);
        parser.next();
        return product;
    }

    /**
     * Read the list of promotions using the specified XML pull parser.
     *
     * @param parser the XML parser containing the promotions to read.
     */
    private List<Promotion> readPromotions(Context context, XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, null, Constants.XML_TAG_PROMOTIONS);
        List<Promotion> promotions = new ArrayList<Promotion>();
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals(Constants.XML_TAG_PROMOTION)) {
                Promotion promotion = readPromotion(context, parser);
                if (promotion != null)
                    promotions.add(promotion);
            } else {
                skip(parser);
            }
        }
        return promotions;
    }

    /**
     * Read a promotion using the specified XML pull parser.
     *
     * @param parser the XML parser containing the promotion to read.
     */
    private Promotion readPromotion(Context context, XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, null, Constants.XML_TAG_PROMOTION);
        String id = parser.getAttributeValue(null, Constants.XML_ATTRIBUTE_ID);
        String name = parser.getAttributeValue(null, Constants.XML_ATTRIBUTE_NAME);
        String thumbnail = parser.getAttributeValue(null, Constants.XML_ATTRIBUTE_THUMBNAIL);
        String image = parser.getAttributeValue(null, Constants.XML_ATTRIBUTE_LARGE_IMAGE);
        float price = Float.parseFloat(parser.getAttributeValue(null, Constants.XML_ATTRIBUTE_PRICE));
        parser.next();
        return new Promotion(id, name, thumbnail, image, price);
    }

    /**
     * Skip the current item in the XML parser.
     *
     * @param parser the XML parser for which to skip the current item
     * @throws XmlPullParserException throws an exception if the current tag isn't a START_TAG.
     * @throws IOException
     */
    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
