package com.ensighten.demo.debug;

import android.util.Log;

import com.ensighten.demo.constants.Constants;

/**
 * Class used for printing debug statements to logcat.
 */
public class Debug {

    /**
     * Print an error log containing a message and the stack trace of the specified exception.
     *
     * @param message   the error message to print.
     * @param exception the exception for which to print the stack trace in debug.
     */
    public static void printError(String message, Exception exception) {
        Log.e(Constants.LOGCAT_TAG, message, exception);
    }

    /**
     * The set of store manager messages that can be printed.
     */
    public static class StoreManagerMessages {
        public static final String ERROR_UNABLE_TO_READ_STORE = "Unable to read the store from store.xml file.";
    }

}
