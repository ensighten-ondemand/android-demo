package com.ensighten.demo.ui.privacy;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.widget.TextView;

import com.ensighten.demo.R;
import com.ensighten.demo.constants.Constants;
import com.ensighten.demo.ui.utils.UIUtils;

/**
 * Activity that contains the IAB preferences.
 */
public class PrivacyIABActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_iab);

        ActionBar actionBar = getSupportActionBar();
        if ( actionBar != null ) {
            actionBar.setDisplayHomeAsUpEnabled( true );
            actionBar.setHomeButtonEnabled( true );
        }

        Context context = getApplicationContext();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        boolean cmpPresent = preferences.getBoolean(getString(R.string.privacy_iab_consent_cmp_present), false);

        TextView cmpPresentTextView = findViewById(R.id.privacy_iab_cmp_present);
        cmpPresentTextView.setText(Boolean.toString(cmpPresent));

        if (cmpPresent) {
            TextView subjectToGDRPTextView = findViewById(R.id.privacy_iab_subject_to_gdpr);
            subjectToGDRPTextView.setText(preferences.getString(getString(R.string.privacy_iab_consent_subject_to_gdrp), Constants.ZERO));

            TextView consentStringTextView = findViewById(R.id.privacy_iab_consent_string);
            consentStringTextView.setText(preferences.getString(getString(R.string.privacy_iab_consent_consent_string), getString(R.string.not_available)));

            TextView parsedPurposeConstantsTextView = findViewById(R.id.privacy_iab_parsed_purpose_consents);
            parsedPurposeConstantsTextView.setText(preferences.getString(getString(R.string.privacy_iab_consent_parsed_purpose_consents), getString(R.string.not_available)));

            TextView parsedVendorConstantsTextView = findViewById(R.id.privacy_iab_parsed_vendor_consents);
            parsedVendorConstantsTextView.setText(preferences.getString(getString(R.string.privacy_iab_consent_parsed_vendor_consents), getString(R.string.not_available)));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}