package com.ensighten.demo.ui.privacy.custom.kotlin

import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.ensighten.Ensighten
import com.ensighten.demo.R
import com.ensighten.privacy.PrivacyManager
import com.ensighten.privacy.model.PrivacyTranslation
import com.google.android.material.snackbar.Snackbar

class CustomPrivacyBanner : PrivacyManager.PrivacyBanner {

    /**
     * The snackbar used as the custom privacy banner.
     */
    private lateinit var mSnackbar: Snackbar

    override fun open(translation: PrivacyTranslation?) {
        val activity = Ensighten.getCurrentActivity()
        if (activity != null) {
            var view = activity.findViewById(android.R.id.content) as View
            if (view != null) {
                mSnackbar = Snackbar.make(view, translation!!.bannerContent, Snackbar.LENGTH_INDEFINITE).setAction(translation!!.bannerDismiss) { v -> Ensighten.getPrivacyManager().openModal() }
                (mSnackbar.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView).maxLines = 10
                val textView = mSnackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text) as TextView
                textView.typeface = ResourcesCompat.getFont(activity, R.font.lato_light);
                val actionTextView = mSnackbar.getView().findViewById(com.google.android.material.R.id.snackbar_action) as TextView
                actionTextView.setBackgroundColor(ContextCompat.getColor(activity, R.color.ensightenBlue))
                actionTextView.setTextColor(Color.WHITE)
                actionTextView.typeface = ResourcesCompat.getFont(activity, R.font.rajdhani_semibold);
                actionTextView.setPadding(30, 0, 30, 0)
                mSnackbar.show()
            }
        }

    }

    override fun close() {
        mSnackbar?.dismiss()
    }

}