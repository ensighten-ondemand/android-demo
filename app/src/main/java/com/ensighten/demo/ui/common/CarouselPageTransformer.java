package com.ensighten.demo.ui.common;

import androidx.viewpager.widget.ViewPager;
import android.view.View;

/**
 * Class used to provide the carousel effect to view pagers.
 */
public class CarouselPageTransformer implements ViewPager.PageTransformer {
    @Override
    public void transformPage(View page, float position) {
        float scaleFactor = 0.15f;

        if (position < 0) {
            float scale = 1 + scaleFactor * position;
            page.setScaleX(scale);
            page.setScaleY(scale);

        } else {
            float scale = 1 - scaleFactor * position;
            page.setScaleX(scale);
            page.setScaleY(scale);
        }
    }
}
