package com.ensighten.demo.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.ensighten.Ensighten
import com.ensighten.demo.R
import com.ensighten.demo.constants.Constants
import com.ensighten.demo.store.StoreManager
import com.ensighten.demo.ui.common.CarouselPageTransformer
import com.ensighten.demo.ui.common.HeaderGridView

/**
 * The base activity that handles transition animations.
 */
open class BaseActivity : AppCompatActivity() {

    override fun startActivity(intent: Intent?) {
        super.startActivity(intent)
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.activity_back_enter, R.anim.activity_back_exit);
    }

    /**
     * Start the home activity
     */
    fun startHomeActivity() {
        startActivity(Intent(baseContext, HomeActivity::class.java))
    }

    /**
     * Start the cart activity that shows all the items currently in the shopping cart.
     */
    fun startCartActivity() {
        startActivity(Intent(baseContext, CartActivity::class.java))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}