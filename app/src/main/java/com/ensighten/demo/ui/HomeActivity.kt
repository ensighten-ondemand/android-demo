package com.ensighten.demo.ui

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.ensighten.Ensighten
import com.ensighten.demo.R
import com.ensighten.demo.constants.Constants
import com.ensighten.demo.store.StoreManager
import com.ensighten.demo.tracker.DemoTracker
import com.ensighten.demo.ui.common.CarouselPageTransformer
import com.ensighten.demo.ui.common.HeaderGridView
import com.ensighten.demo.ui.privacy.PrivacyActivity
import com.ensighten.demo.ui.tools.ToolsActivity
import com.ensighten.demo.ui.utils.UIUtils
import com.ensighten.demo.utilities.Utilities
import com.google.android.material.navigation.NavigationView
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import java.text.NumberFormat


/**
 * The main home activity that shows the list of promotions and items available in the store.
 */
class HomeActivity : BaseActivity() {

    /**
     * The pager adapter of promotions that are available at the store.
     */
    private lateinit var mPromotionPagerAdapter: PromotionPagerAdapter

    /**
     * The adapter of products that are available in the store.
     */
    private lateinit var mProductAdapter: ProductAdapter

    /**
     * The drawer layout for the navigation view.
     */
    private lateinit var mDrawerLayout: DrawerLayout


    companion object {
        private const val ARG_PROMOTION_ID = "promotionId"
    }

    /**
     * Create the home activity and populate the promotions and products.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        WebView.setWebContentsDebuggingEnabled(true)

        Ensighten.setTracker(DemoTracker())
        Ensighten.bootstrap(this, getString(R.string.ensighten_account_id), getString(R.string.ensighten_app_id))
        Ensighten.addNativeBridgeListener("GoogleClientId") { value : String ->  Log.d("tstatic","Google Client Id = " + value); null}
//        Ensighten.setOnJavascriptReadyListener {
//            Log.d("tstatic","onJavascriptReady")
//            Ensighten.getNetworkManager().a("Bootstrapper.googleClientId", String::class.java, { value ->  Log.d("tstatic","Google Client Id = " + value)})
//        }

        Utilities.updatePrivacyTheme(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val toolbar = findViewById<Toolbar>(R.id.toolbar);
        toolbar.setTitle(R.string.title_home)
        setSupportActionBar(toolbar)

        val actionBar: ActionBar? = supportActionBar
        actionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.navigation_menu)
        }

        mDrawerLayout = findViewById(R.id.drawer_layout)

        val navigationView: NavigationView = findViewById(R.id.navigation_view)
        navigationView.setNavigationItemSelectedListener { item ->

            when (item.itemId) {
                R.id.navigation_cart -> {
                    startCartActivity();
                }
                R.id.navigation_about -> {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.url_ensighten_mobile))))
                }
                R.id.navigation_contact_us -> {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.url_contact_us))))
                }
                R.id.navigation_privacy -> {
                    startActivity(Intent(baseContext, PrivacyActivity::class.java))
                }
                R.id.navigation_tools -> {
                    startActivity(Intent(baseContext, ToolsActivity::class.java))
                }
            }

            item.setChecked(false)

            mDrawerLayout.closeDrawers()

            true
        }

        mPromotionPagerAdapter = PromotionPagerAdapter(this, supportFragmentManager)

        mProductAdapter = ProductAdapter(this)
        val productsGridView = findViewById<HeaderGridView>(R.id.products);
        val promotionsPager = getLayoutInflater().inflate(R.layout.fragment_promotions_view_pager, productsGridView, false) as ViewPager
        promotionsPager.adapter = mPromotionPagerAdapter
        promotionsPager.setPageTransformer(true, CarouselPageTransformer())
        promotionsPager.clipToPadding = false
        promotionsPager.setPadding(resources.getDimension(R.dimen.promotion_padding_horizontal).toInt(), resources.getDimension(R.dimen.promotion_padding_vertical).toInt(), resources.getDimension(R.dimen.promotion_padding_horizontal).toInt(), resources.getDimension(R.dimen.promotion_padding_vertical).toInt())
        productsGridView.addHeaderView(promotionsPager)
        productsGridView.adapter = mProductAdapter
        productsGridView.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val intent = Intent(baseContext, ProductDetailActivity::class.java)
                intent.putExtra(Constants.TAG_PRODUCT_ID, view.tag.toString())
                startActivity(intent)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val store = StoreManager.getInstance(this).store;
        val promotions = store.promotions;
        val products = store.products;
        var gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
        var data = JsonObject()
        data.add(Constants.JSON_PROMOTIONS, gson.toJsonTree(promotions))
        data.add(Constants.JSON_PRODUCTS, gson.toJsonTree(products))
        Ensighten.trackPageView(Constants.HOME_PAGE, data);
    }

    override fun onCreateOptionsMenu(menu : Menu): Boolean {
        menuInflater.inflate(R.menu.home, menu)

        for(i in 0 until menu.size()) {
            val drawable = menu.getItem(i).getIcon()
            drawable?.mutate()
            drawable?.setColorFilter(resources.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        }

        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                mDrawerLayout.openDrawer(GravityCompat.START)
                true
            }
            R.id.menu_cart -> {
                startCartActivity();
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * The pager adapter that populates all the store promotions.
     */
    class PromotionPagerAdapter(context: Context, fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        /**
         * The context of the application.
         */
        var mContext = context

        /**
         * The list of promotions available at the store.
         */
        var mPromotions = StoreManager.getInstance(mContext).store.promotions

        override fun getCount(): Int  = mPromotions.size

        override fun getItem(i: Int): Fragment {
            val fragment = PromotionFragment()
            fragment.arguments = Bundle().apply {
                val promotion = mPromotions.get(i)
                putString(ARG_PROMOTION_ID, promotion.id)
            }
            return fragment
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mPromotions.get(position).name
        }
    }

    /**
     * The products adapter that populates all the store products.
     */
    class ProductAdapter(context: Context) : BaseAdapter() {

        /**
         * The context of the application.
         */
        var context = context

        /**
         * The list of products available in the store.
         */
        var mProducts = StoreManager.getInstance(this.context).store.products

        override fun getCount() = mProducts.size

        override fun getItemId(position: Int) = mProducts.get(position).id.toLong()

        override fun getItem(position: Int) = mProducts.get(position)

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            var productLayout = convertView;
            if (productLayout == null) {
                val inflater = LayoutInflater.from(context)
                productLayout = inflater.inflate(
                        R.layout.view_product, null, false)
            }

            val product = mProducts.get(position)

            val productImage = productLayout?.findViewById<ImageView>(R.id.productImage)
            productImage?.setImageResource(Utilities.getImageIdByName(context, product.thumbnail));

            val productPrice = productLayout?.findViewById<TextView>(R.id.productPrice)
            productPrice?.setText(NumberFormat.getCurrencyInstance().format(product.price))

            productLayout?.tag = mProducts.get(position).id

            return productLayout
        }
    }

    /**
     * The fragment that contains a store promotion.
     */
    class PromotionFragment() : Fragment() {

        override fun onCreateView(inflater: LayoutInflater,
                                  container: ViewGroup?,
                                  savedInstanceState: Bundle?): View {
            val promotionLayout: View = inflater.inflate(
                    R.layout.fragment_promotion, null, false)

            arguments?.takeIf { it.containsKey(ARG_PROMOTION_ID) }?.apply {
                val promotion = StoreManager.getInstance(this@PromotionFragment.context).store.getPromotionById(getString(ARG_PROMOTION_ID))

                val promotionThumbnail = promotionLayout.findViewById<ImageView>(R.id.promotionThumbnail)
                promotionThumbnail.setImageResource(Utilities.getImageIdByName(this@PromotionFragment.context, promotion.thumbnail));

                val promotionPrice = promotionLayout.findViewById<TextView>(R.id.promotionPrice)
                promotionPrice.setText(NumberFormat.getCurrencyInstance().format(promotion.price))

                promotionLayout?.setOnClickListener {
                    var dialog = Dialog(context!!)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog.setContentView(R.layout.dialog_promotion);
                    dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

                    val promotionImage = dialog.findViewById<ImageView>(R.id.promotionImage);
                    promotionImage.setImageResource(Utilities.getImageIdByName(this@PromotionFragment.context, promotion.image))

                    val addToCartButton = dialog.findViewById<Button>(R.id.addToCart)
                    addToCartButton.setOnClickListener {
                        UIUtils.addToCart(this@PromotionFragment.activity as BaseActivity, promotion, object: UIUtils.CartDialogListener {
                            override fun goToCart() {
                                dialog.dismiss()
                                (activity as BaseActivity).startCartActivity()
                            }

                            override fun continueShopping() {
                                dialog.dismiss()
                            }
                        })
                        val data = java.util.HashMap<String, String>()
                        data.put(Constants.JSON_PROMOTION_ID, promotion.id)
                        data.put(Constants.JSON_PROMOTION_NAME, promotion.name)
                        data.put(Constants.JSON_PROMOTION_PRICE, NumberFormat.getCurrencyInstance().format(promotion.price.toDouble()))
                        Ensighten.trackConversion(Constants.PROMOTION_ADDED_TO_CART_CONVERSION, data)
                    }

                    val cancelButton = dialog.findViewById<ImageButton>(R.id.cancel)
                    cancelButton.setOnClickListener {
                        dialog.dismiss()
                    }

                    dialog.show()

                    val data = HashMap<String, String>()
                    data.put(Constants.JSON_PROMOTION_ID, promotion.id)
                    data.put(Constants.JSON_PROMOTION_NAME, promotion.name)
                    data.put(Constants.JSON_PROMOTION_PRICE, NumberFormat.getCurrencyInstance().format(promotion.price))
                    Ensighten.trackPageView(Constants.PROMOTION_DETAIL_PAGE, data);
                }
            }

            return promotionLayout
        }
    }
}
