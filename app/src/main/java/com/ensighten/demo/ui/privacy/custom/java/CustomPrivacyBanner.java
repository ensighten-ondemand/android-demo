package com.ensighten.demo.ui.privacy.custom.java;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.ensighten.Ensighten;
import com.ensighten.demo.R;
import com.ensighten.privacy.PrivacyManager;
import com.ensighten.privacy.model.PrivacyTranslation;
import com.google.android.material.snackbar.Snackbar;

public class CustomPrivacyBanner implements PrivacyManager.PrivacyBanner {

        /**
         * The snackbar used as the custom privacy banner.
         */
        private Snackbar mSnackbar;

        @Override
        public void open(PrivacyTranslation translation) {
            Activity activity = Ensighten.getCurrentActivity();
            if (activity != null) {
                View view = activity.findViewById(android.R.id.content);
                if (view != null) {
                    mSnackbar = Snackbar.make(view, translation.getBannerContent(), Snackbar.LENGTH_INDEFINITE).setAction(translation.getBannerDismiss(), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Ensighten.getPrivacyManager().openModal();
                                }
                            }
                    );
                    TextView textView = mSnackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setMaxLines(10);
                    textView.setTypeface(ResourcesCompat.getFont(activity, R.font.lato_light));
                    TextView actionTextView = (TextView) mSnackbar.getView().findViewById(com.google.android.material.R.id.snackbar_action);
                    actionTextView.setBackgroundColor(ContextCompat.getColor(activity, R.color.ensightenBlue));
                    actionTextView.setTextColor(Color.WHITE);
                    actionTextView.setTypeface(ResourcesCompat.getFont(activity, R.font.rajdhani_semibold));
                    actionTextView.setPadding(30, 0, 30, 0);
                    mSnackbar.show();
                }
            }
        }

        @Override
        public void close() {
            if (mSnackbar != null) {
                mSnackbar.dismiss();
            }
        }

}
