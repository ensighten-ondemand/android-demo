package com.ensighten.demo.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ensighten.Ensighten;
import com.ensighten.demo.R;
import com.ensighten.demo.constants.Constants;

/**
 * Activity that shows the successful checkout screen.
 */
public class CheckoutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        Button goToStoreButton = findViewById(R.id.goToStore);
        goToStoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CheckoutActivity.this, HomeActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Ensighten.trackPageView(Constants.CHECKOUT_COMPLETE_PAGE);
    }

}
