package com.ensighten.demo.ui.tools;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;

import com.ensighten.demo.R;
import com.ensighten.demo.ui.utils.UIUtils;

/**
 * Activity that contains different tools for testing Ensighten, such as as the ability to test
 * deep links and app crashes.
 */
public class ToolsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if ( actionBar != null ) {
            actionBar.setDisplayHomeAsUpEnabled( true );
            actionBar.setHomeButtonEnabled( true );
        }

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new ToolsFragment())
                .commit();
    }

    public static class ToolsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.tools);

            Preference testDeepLinkTrackingPreference = findPreference(getString(R.string.tools_test_deep_link_tracking_key));
            testDeepLinkTrackingPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    UIUtils.Companion.showTestDeepLinkTrackingDialog(getActivity());
                    return true;
                }
            });

            Preference testAppCrashTrackingPreference = findPreference(getString(R.string.tools_test_app_crash_tracking_key));
            testAppCrashTrackingPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    UIUtils.Companion.showAppCrashTrackingDialog(getActivity());
                    return true;
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
