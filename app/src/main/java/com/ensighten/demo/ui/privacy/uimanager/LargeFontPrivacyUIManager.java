package com.ensighten.demo.ui.privacy.uimanager;

import com.ensighten.privacy.PrivacyUIManager;

/**
 * Class used to set the large font theme for the privacy modal.
 */
public class LargeFontPrivacyUIManager extends PrivacyUIManager {

    public Float getPrivacyModalTitleTextSize() {
        return 24f;
    }

    public Float getPrivacyModalDescriptionTextSize() {
        return 18f;
    }

    public Float getPrivacyModalCategoryTitleTextSize() {
        return 22f;
    }

    public Float getPrivacyModalCategoryDescriptionTextSize() {
        return 18f;
    }

    public Float getPrivacyModalSaveButtonTextSize() {
        return 18f;
    }

    public Float getPrivacyModalCancelButtonTextSize() {
        return 18f;
    }
}

