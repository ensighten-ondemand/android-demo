package com.ensighten.demo.ui.privacy.uimanager;

import android.content.Context;
import android.graphics.Color;

import com.ensighten.demo.R;
import com.ensighten.privacy.PrivacyUIManager;

/**
 * Class used to set the dark theme for the privacy modal.
 */
public class DarkThemePrivacyUIManager extends PrivacyUIManager {

    private final Context mContext;

    public DarkThemePrivacyUIManager(Context context) {
        mContext = context;
    }

    public Integer getPrivacyModalBackgroundColor() {
        return Color.BLACK;
    }

    public Integer getPrivacyModalSeparatorColor() {
        return mContext.getResources().getColor(R.color.orange);
    }

    public Integer getPrivacyModalCategoryTitleTextColor() {
        return mContext.getResources().getColor(R.color.orange);
    }

    public Integer getPrivacyModalCategoryDescriptionTextColor() {
        return mContext.getResources().getColor(R.color.orange);
    }

    public Integer getPrivacyModalSwitchColor() {
        return mContext.getResources().getColor(R.color.dark_orange);
    }

    public Integer getPrivacyModalDescriptionTextColor() {
        return mContext.getResources().getColor(R.color.orange);
    }

    public Integer getPrivacyModalTitleTextColor() {
        return mContext.getResources().getColor(R.color.orange);
    }

    public Integer getPrivacyModalCancelButtonTextColor() {
        return mContext.getResources().getColor(R.color.orange);
    }

    public Integer getPrivacyModalSaveButtonTextColor() {
        return mContext.getResources().getColor(R.color.orange);
    }
}

