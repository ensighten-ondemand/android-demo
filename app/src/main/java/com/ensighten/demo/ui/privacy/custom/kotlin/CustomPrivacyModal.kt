package com.ensighten.demo.ui.privacy.custom.kotlin

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.LightingColorFilter
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ensighten.Ensighten
import com.ensighten.demo.R
import com.ensighten.demo.constants.Constants
import com.ensighten.demo.ui.privacy.custom.kotlin.CustomPrivacyCookiesAdapter
import com.ensighten.privacy.PrivacyManager
import com.ensighten.privacy.model.PrivacyCookie
import com.ensighten.privacy.model.PrivacyTranslation
import com.ensighten.privacy.ui.SeparatorDecoration
import com.ensighten.utils.Utils
import java.util.*

/**
 * Class representing a custom privacy modal that shows the cookie policy and other disclosures to end
 * users.
 */
class CustomPrivacyModal : DialogFragment(), PrivacyManager.PrivacyModal {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val privacyManager = Ensighten.getPrivacyManager()
        val translation = arguments?.getSerializable(Constants.KEY_PRIVACY_TRANSLATION) as PrivacyTranslation
        val cookies = arguments?.getSerializable(Constants.KEY_PRIVACY_COOKIES) as ArrayList<PrivacyCookie>

        val privacyModal = activity?.layoutInflater?.inflate(R.layout.privacy_modal_custom, null)
        val translationConsentDescription = translation.consentDescription
        val translationConsentTitle = translation.consentTitle
        val translationSave = translation.save
        val translationCancel = translation.cancel
        val description = privacyModal?.findViewById<TextView>(R.id.privacy_modal_description)
        description?.text = Utils.fromHtml(translationConsentDescription)
        description?.movementMethod = LinkMovementMethod.getInstance()

        val cookiesList = privacyModal?.findViewById<View>(R.id.privacy_modal_cookies) as RecyclerView
        val layoutManager = LinearLayoutManager(activity)
        cookiesList.setHasFixedSize(true)
        val separatorDecorationBuilder = SeparatorDecoration.Builder(activity!!.applicationContext)

        cookiesList.layoutManager = layoutManager
        cookiesList.addItemDecoration(separatorDecorationBuilder.build())
        cookiesList.adapter = CustomPrivacyCookiesAdapter(cookies)

        val consentTitleLayout = activity!!.layoutInflater.inflate(R.layout.privacy_modal_title_custom, null)
        val consentTitle = consentTitleLayout.findViewById<TextView>(R.id.privacy_modal_title)
        consentTitle.text = translation.consentTitle

        val builder = AlertDialog.Builder(activity)
        builder.setCustomTitle(consentTitleLayout)
                .setView(privacyModal)
                .setPositiveButton(translationSave) { dialog, id -> privacyManager.setCookies(cookies) { privacyManager.updatePreferences() } }
                .setNegativeButton(translationCancel) { dialog, id -> }.setCancelable(false)

        return builder.create()
    }

    override fun onStart()
    {
        super.onStart()

        dialog?.getWindow()!!.decorView.background.colorFilter = LightingColorFilter(Constants.PRIVACY_CUSTOM_DIALOG_BACKGROUND_COLOR_FILTER_MASK, ContextCompat.getColor(context!!, R.color.ensightenBlack))

        val saveButton = (dialog as AlertDialog).getButton(DialogInterface.BUTTON_POSITIVE)
        saveButton?.setTextColor(Color.WHITE)
        saveButton?.typeface = ResourcesCompat.getFont(context!!, R.font.rajdhani_semibold);

        val cancelButton = (dialog as AlertDialog).getButton(DialogInterface.BUTTON_NEGATIVE)
        cancelButton?.setTextColor(Color.WHITE)
        cancelButton?.typeface = ResourcesCompat.getFont(context!!, R.font.rajdhani_semibold);
    }

    override fun onDismiss(dialog: DialogInterface) {
        Ensighten.getPrivacyManager().notifyBannerClosed()
        super.onDismiss(dialog)
    }

    /**
     * Display a privacy modal that shows the options used for opting in and out of cookies.
     *
     * @param translation the translation used to populate the dialog text
     * @param cookies the cookies that the user can opt-in and out of.
     */
    override fun open(translation: PrivacyTranslation, cookies: ArrayList<PrivacyCookie>) {
        val activity = Ensighten.getCurrentActivity()
        val args = Bundle()
        args.putSerializable(Constants.KEY_PRIVACY_TRANSLATION, translation)
        args.putSerializable(Constants.KEY_PRIVACY_COOKIES, cookies)
        arguments = args

        val supportFragmentManager = (activity as AppCompatActivity).supportFragmentManager
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag(Constants.PRIVACY_MODAL_TAG)
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        show(fragmentTransaction, Constants.PRIVACY_MODAL_TAG)
        supportFragmentManager.executePendingTransactions()
    }

    /**
     * Close the privacy modal that shows the options used for opting in and out of cookies.
     */
    override fun close() {
        dismiss()
    }

}