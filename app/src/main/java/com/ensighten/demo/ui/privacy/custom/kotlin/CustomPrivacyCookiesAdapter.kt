package com.ensighten.demo.ui.privacy.custom.kotlin

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ensighten.demo.R
import com.ensighten.privacy.model.PrivacyCookie
import com.ensighten.utils.Utils

/**
 * Adapter used for displaying privacy cookies in a list.
 */
class CustomPrivacyCookiesAdapter(cookies: List<PrivacyCookie>) : RecyclerView.Adapter<CustomPrivacyCookiesAdapter.ViewHolder>() {

    /**
     * The list of privacy cookies to display in this adapter.
     */
    private val mCookies = cookies

    /**
     * View holder that contains references to all the UI elements present in a single row.
     */
    class ViewHolder(cookieLayout: ConstraintLayout) : RecyclerView.ViewHolder(cookieLayout) {

        /**
         * The cookie title text view.
         */
        var mCookieTitleTextView: TextView

        /**
         * The cookie description text view.
         */
        var mCookieDescriptionTextView: TextView

        /**
         * The cookie switch.
         */
        var mCookieSwitch: Switch

        init {

            mCookieTitleTextView = cookieLayout.findViewById<View>(R.id.cookie_title) as TextView
            mCookieDescriptionTextView = cookieLayout.findViewById<View>(R.id.cookie_description) as TextView
            mCookieSwitch = cookieLayout.findViewById<View>(R.id.cookie_switch) as Switch

            val ensightenBlue = ContextCompat.getColor(cookieLayout.context, R.color.ensightenBlue)
            val ensightenGray = ContextCompat.getColor(cookieLayout.context, R.color.ensightenGray)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val thumbStates = ColorStateList(
                        arrayOf(intArrayOf()),
                        intArrayOf(Color.WHITE)
                )
                mCookieSwitch.thumbTintList = thumbStates
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                val trackStates = ColorStateList(
                        arrayOf(intArrayOf(-android.R.attr.state_enabled), intArrayOf(-android.R.attr.state_checked), intArrayOf()),
                        intArrayOf(ensightenGray, ensightenGray, ensightenBlue)
                )
                mCookieSwitch.trackTintList = trackStates
                mCookieSwitch.trackTintMode = PorterDuff.Mode.OVERLAY
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val cookieLayout = LayoutInflater.from(parent.context)
                .inflate(R.layout.privacy_modal_cookie_custom, parent, false) as ConstraintLayout
        return ViewHolder(cookieLayout)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cookie = mCookies[position]
        holder.mCookieTitleTextView.text = Utils.fromHtml(cookie.title)
        holder.mCookieTitleTextView.movementMethod = LinkMovementMethod.getInstance()
        holder.mCookieDescriptionTextView.text = Utils.fromHtml(cookie.description)
        holder.mCookieDescriptionTextView.movementMethod = LinkMovementMethod.getInstance()
        holder.mCookieSwitch.isChecked = cookie.valueAsBoolean!!
        if (cookie.isEditable) {
            holder.mCookieSwitch.setOnCheckedChangeListener { buttonView, isChecked -> cookie.setValue(isChecked) }
            holder.mCookieSwitch.visibility = View.VISIBLE
        } else {
            holder.mCookieSwitch.visibility = View.INVISIBLE
        }
    }

    override fun getItemCount(): Int {
        return mCookies.size
    }
}