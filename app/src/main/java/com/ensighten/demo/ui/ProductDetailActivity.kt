package com.ensighten.demo.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.ensighten.Ensighten
import com.ensighten.demo.R
import com.ensighten.demo.constants.Constants
import com.ensighten.demo.model.Product
import com.ensighten.demo.store.StoreManager
import com.ensighten.demo.ui.common.HorizontalListView
import com.ensighten.demo.ui.utils.UIUtils
import com.ensighten.demo.utilities.Utilities
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import java.text.NumberFormat


class ProductDetailActivity : BaseActivity() {

    // The product
    private lateinit var product : Product

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true);
        supportActionBar?.setDisplayShowHomeEnabled(true);

        val productId: String = intent.getStringExtra(Constants.TAG_PRODUCT_ID)!!
        product = StoreManager.getInstance(this).store.getProductById(productId)
        if (product == null) {
            finish()
        }

        val productsGridView = findViewById<HorizontalListView>(R.id.productImages);
        productsGridView.adapter = ProductImageAdapter(this, product.images)

        val nameView = findViewById<TextView>(R.id.productName);
        nameView?.text = product.name

        val descriptionView = findViewById<TextView>(R.id.productDescription);
        descriptionView?.text = product.description

        val priceView = findViewById<TextView>(R.id.productPrice);
        priceView?.text = NumberFormat.getCurrencyInstance().format(product.price)

        val addToCartButton = findViewById<Button>(R.id.addToCart);
        addToCartButton.setOnClickListener {
            UIUtils.addToCart(this@ProductDetailActivity, product, object: UIUtils.CartDialogListener {
                override fun goToCart() {
                    finish()
                    startCartActivity()
                }

                override fun continueShopping() {
                    finish()
                }
            })
        }
    }

    override fun onResume() {
        super.onResume()
        val data = HashMap<String, String>()
        data.put(Constants.JSON_PRODUCT_ID, product.id)
        data.put(Constants.JSON_PRODUCT_NAME, product.name)
        data.put(Constants.JSON_PRODUCT_PRICE, NumberFormat.getCurrencyInstance().format(product.price))
        Ensighten.trackPageView(Constants.PRODUCT_DETAIL_PAGE, data);
    }

    /**
     * The product images adapter that populates all the product images.
     */
    class ProductImageAdapter(context: Context, productImages: List<String>) : BaseAdapter() {

        internal class ViewHolder {
            var productImage: ImageView? = null
        }

        /**
         * The context of the application.
         */
        var context = context

        /**
         * The list of product images
         */
        var productImages = productImages

        override fun getCount() = productImages.size

        override fun getItemId(position: Int) = productImages.get(position).toLong()

        override fun getItem(position: Int) = productImages.get(position)

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            var productImage = convertView
            if (productImage == null) {
                val inflater = LayoutInflater.from(context)
                val holder = ViewHolder()
                productImage = inflater.inflate(
                        R.layout.view_product_image, null, false) as ImageView
                holder.productImage = productImage
                productImage?.tag = holder
            }

            (productImage?.tag as ViewHolder).productImage?.setImageResource(Utilities.getImageIdByName(context, productImages.get(position)))

            return productImage
        }
    }

}
