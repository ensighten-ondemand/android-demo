package com.ensighten.demo.ui.privacy.custom.java;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ensighten.Ensighten;
import com.ensighten.demo.R;
import com.ensighten.demo.constants.Constants;
import com.ensighten.privacy.PrivacyManager;
import com.ensighten.privacy.model.PrivacyCookie;
import com.ensighten.privacy.model.PrivacyTranslation;
import com.ensighten.privacy.ui.SeparatorDecoration;
import com.ensighten.utils.Utils;

import java.util.ArrayList;

/**
 * Class representing a custom privacy modal that shows the cookie policy and other disclosures to end
 * users.
 */
public class CustomPrivacyModal extends DialogFragment implements PrivacyManager.PrivacyModal {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        PrivacyManager privacyManager = Ensighten.getPrivacyManager();
        PrivacyTranslation translation = (PrivacyTranslation) getArguments().getSerializable(Constants.KEY_PRIVACY_TRANSLATION);
        final ArrayList<PrivacyCookie> cookies = (ArrayList<PrivacyCookie>) getArguments().getSerializable(Constants.KEY_PRIVACY_COOKIES);

        View privacyModal = getActivity().getLayoutInflater().inflate(R.layout.privacy_modal_custom, null);
        String translationConsentDescription = translation.getConsentDescription();
        String translationConsentTitle = translation.getConsentTitle();
        String translationSave = translation.getSave();
        String translationCancel = translation.getCancel();
        TextView description = (TextView) privacyModal.findViewById(R.id.privacy_modal_description);
        if (description != null) {
            description.setText(Utils.fromHtml(translationConsentDescription));
            description.setMovementMethod(LinkMovementMethod.getInstance());
        }

        RecyclerView cookiesList = (RecyclerView) privacyModal.findViewById(R.id.privacy_modal_cookies);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        cookiesList.setHasFixedSize(true);
        SeparatorDecoration.Builder separatorDecorationBuilder = new SeparatorDecoration.Builder(getActivity().getApplicationContext());
        cookiesList.setLayoutManager(layoutManager);
        cookiesList.addItemDecoration(separatorDecorationBuilder.build());
        cookiesList.setAdapter(new CustomPrivacyCookiesAdapter(cookies));

        View consentTitleLayout = getActivity().getLayoutInflater().inflate(R.layout.privacy_modal_title_custom, null);
        TextView consentTitle = consentTitleLayout.findViewById(R.id.privacy_modal_title);
        consentTitle.setText(translation.getConsentTitle());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCustomTitle(consentTitleLayout)
        .setView(privacyModal)
        .setPositiveButton(translationSave, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                privacyManager.setCookies(cookies, new ValueCallback<Void>() {
                    @Override
                    public void onReceiveValue(Void value) {
                        privacyManager.updatePreferences();
                    }
                });
                dismiss();
            }
        }).setNegativeButton(translationCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        })
        .setCancelable(false);

        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().getDecorView().getBackground().setColorFilter(new LightingColorFilter(Constants.PRIVACY_CUSTOM_DIALOG_BACKGROUND_COLOR_FILTER_MASK, ContextCompat.getColor(getContext(), R.color.ensightenBlack)));

        Button saveButton = ((AlertDialog) getDialog()).getButton(DialogInterface.BUTTON_POSITIVE);
        saveButton.setTextColor(Color.WHITE);
        saveButton.setTypeface(ResourcesCompat.getFont(getContext(), R.font.rajdhani_semibold));

        Button cancelButton = ((AlertDialog) getDialog()).getButton(DialogInterface.BUTTON_NEGATIVE);
        cancelButton.setTextColor(Color.WHITE);
        cancelButton.setTypeface(ResourcesCompat.getFont(getContext(), R.font.rajdhani_semibold));
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Ensighten.getPrivacyManager().notifyBannerClosed();
    }

    /**
     * Display a privacy modal that shows the options used for opting in and out of cookies.
     *
     * @param translation the translation used to populate the dialog text
     * @param cookies the cookies that the user can opt-in and out of.
     */
    @Override
    public void open(PrivacyTranslation translation, ArrayList<PrivacyCookie> cookies) {
        Activity activity = Ensighten.getCurrentActivity();
        Bundle args = new Bundle();
        args.putSerializable(Constants.KEY_PRIVACY_TRANSLATION, translation);
        args.putSerializable(Constants.KEY_PRIVACY_COOKIES, cookies);
        setArguments(args);
        FragmentManager fragmentManager = ((AppCompatActivity) activity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment prev = fragmentManager.findFragmentByTag(Constants.PRIVACY_MODAL_TAG);
        if (prev != null) {
            fragmentTransaction.remove(prev);
        }
        fragmentTransaction.disallowAddToBackStack();
        show(fragmentTransaction, Constants.PRIVACY_MODAL_TAG);
    }

    /**
     * Close the privacy modal that shows the options used for opting in and out of cookies.
     */
    public void close() {
        dismiss();
    }
}
