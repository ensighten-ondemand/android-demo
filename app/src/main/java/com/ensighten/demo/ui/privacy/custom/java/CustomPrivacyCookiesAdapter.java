package com.ensighten.demo.ui.privacy.custom.java;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.ensighten.demo.R;
import com.ensighten.privacy.model.PrivacyCookie;
import com.ensighten.utils.Utils;

import java.util.List;

/**
 * Adapter used for displaying privacy cookies in a list.
 */
public class CustomPrivacyCookiesAdapter extends RecyclerView.Adapter<CustomPrivacyCookiesAdapter.ViewHolder> {

    /**
     * The list of privacy cookies to display in this adapter.
     */
    private final List<PrivacyCookie> mCookies;

    /**
     * View holder that contains references to all the UI elements present in a single row.
     */
    class ViewHolder extends RecyclerView.ViewHolder {

            /**
             * The cookie title text view.
             */
            private TextView mCookieTitleTextView;

            /**
             * The cookie description text view.
             */
            private TextView mCookieDescriptionTextView;

            /**
             * The cookie switch.
             */
            private Switch mCookieSwitch;

            public ViewHolder(View cookieLayout) {
                super(cookieLayout);

                mCookieTitleTextView = (TextView) cookieLayout.findViewById(R.id.cookie_title);
                mCookieDescriptionTextView = (TextView) cookieLayout.findViewById(R.id.cookie_description);
                mCookieSwitch = (Switch) cookieLayout.findViewById(R.id.cookie_switch);

                int ensightenBlue = ContextCompat.getColor(cookieLayout.getContext(), R.color.ensightenBlue);
                int ensightenGray = ContextCompat.getColor(cookieLayout.getContext(), R.color.ensightenGray);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ColorStateList thumbStates = new ColorStateList(
                            new int[][] { new int[] {} },
                            new int[] { Color.WHITE }
                    );
                    mCookieSwitch.setThumbTintList(thumbStates);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    ColorStateList trackStates = new ColorStateList(
                            new int[][] { new int[] { -android.R.attr.state_enabled }, new int[] { -android.R.attr.state_checked }, new int[] {} },
                            new int[] { ensightenGray, ensightenGray, ensightenBlue }
                    );
                    mCookieSwitch.setTrackTintList(trackStates);
                    mCookieSwitch.setTrackTintMode(PorterDuff.Mode.OVERLAY);
                }
            }
    }

    public CustomPrivacyCookiesAdapter(List<PrivacyCookie> cookies) {
        mCookies = cookies;
    }

    @Override
    public CustomPrivacyCookiesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.privacy_modal_cookie_custom, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PrivacyCookie cookie = mCookies.get(position);
        holder.mCookieTitleTextView.setText(Utils.fromHtml(cookie.getTitle()));
        holder.mCookieTitleTextView.setMovementMethod(LinkMovementMethod.getInstance());
        holder.mCookieDescriptionTextView.setText(Utils.fromHtml(cookie.getDescription()));
        holder.mCookieDescriptionTextView.setMovementMethod(LinkMovementMethod.getInstance());
        holder.mCookieSwitch.setChecked(cookie.getValueAsBoolean());
        if (cookie.isEditable()) {
            holder.mCookieSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    cookie.setValue(isChecked);
                }
            });
            holder.mCookieSwitch.setVisibility(View.VISIBLE);
        } else {
            holder.mCookieSwitch.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mCookies.size();
    }
}
