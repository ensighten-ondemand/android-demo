package com.ensighten.demo.ui.privacy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.ensighten.Ensighten;
import com.ensighten.demo.R;
import com.ensighten.demo.ui.utils.UIUtils;
import com.ensighten.demo.utilities.Utilities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

/**
 * Activity that contains privacy policy and settings that are managed using Ensighten Privacy.
 */
public class PrivacyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if ( actionBar != null ) {
            actionBar.setDisplayHomeAsUpEnabled( true );
            actionBar.setHomeButtonEnabled( true );
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new PrivacyFragment())
                .commit();
    }

    public static class PrivacyFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.privacy, rootKey);

            Preference privacyPolicyPreference = findPreference(getString(R.string.privacy_policy_key));
            privacyPolicyPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Ensighten.getPrivacyManager().openBanner();
                    return true;
                }
            });

            Preference privacySettingsPreference = findPreference(getString(R.string.privacy_settings_key));
            privacySettingsPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Ensighten.getPrivacyManager().openModal();
                    return true;
                }
            });

            Preference privacyDoNotSellPreference = findPreference(getString(R.string.privacy_do_not_sell_key));
            privacyDoNotSellPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
//                    Ensighten.getPrivacyManager().openDoNotSellModal();
                    return true;
                }
            });

            Preference privacyIABPreference = findPreference(getString(R.string.privacy_iab_key));
            privacyIABPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    startActivity(new Intent(getActivity(), PrivacyIABActivity.class));
                    return true;
                }
            });

            ListPreference privacyThemePreference = (ListPreference) findPreference(getString(R.string.privacy_theme_key));
            privacyThemePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    Utilities.updatePrivacyTheme(getActivity(), (String) newValue);
                    return true;
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
