package com.ensighten.demo.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ensighten.Ensighten;
import com.ensighten.demo.R;
import com.ensighten.demo.constants.Constants;
import com.ensighten.demo.model.CartItem;
import com.ensighten.demo.ui.common.EmptyRecyclerView;
import com.ensighten.demo.cart.CartManager;
import com.ensighten.demo.utilities.Utilities;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.text.NumberFormat;
import java.util.List;

public class CartActivity extends BaseActivity {

    /**
     * The cart manager.
     */
    private CartManager mCartManager;

    /**
     * The view containing the list of items in the cart.
     */
    private EmptyRecyclerView mCartView;

    /**
     * The adapter used to populate the cart view.
     */
    private RecyclerView.Adapter mAdapter;

    /**
     * The manager that is used to define the layout of the cart.
     */
    private RecyclerView.LayoutManager mLayoutManager;

    /**
     * The checkout button.
     */
    private Button mCheckoutButton;

    /**
     * The button showing the order total
     */
    private TextView mTotalTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        ActionBar actionBar = getSupportActionBar();
        if ( actionBar != null ) {
            actionBar.setDisplayHomeAsUpEnabled( true );
            actionBar.setHomeButtonEnabled( true );
        }

        mCartManager = CartManager.getInstance(this);

        mCartView = findViewById(R.id.cart);
        mCartView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mCartView.setLayoutManager(mLayoutManager);

        mCartView.setEmptyView(findViewById(R.id.empty_view));

        mAdapter = new ItemAdapter();
        mCartView.setAdapter(mAdapter);

        mCheckoutButton = findViewById(R.id.checkout);
        mTotalTextView = findViewById(R.id.total);
        update();
    }

    protected void onResume() {
        super.onResume();
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        JsonObject data = new JsonObject();
        data.add(Constants.JSON_ITEMS, gson.toJsonTree(CartManager.getInstance(this).getItems()));
        Ensighten.trackPageView(Constants.CART_PAGE, data);
    }

    /**
     * Update the view due to a change in the cart, such as an added item quantity.
     */
    private void update() {
        if (mCartManager.isCartEmpty()) {
            mCheckoutButton.setText(R.string.go_to_store);
            mCheckoutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(CartActivity.this, HomeActivity.class));
                }
            });
        } else {
            mCheckoutButton.setText(R.string.checkout);
            mCheckoutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCartManager.checkout();
                    startActivity(new Intent(CartActivity.this, CheckoutActivity.class));
                }
            });
        }

        mTotalTextView.setText( NumberFormat.getCurrencyInstance().format(mCartManager.getCartTotal()));
        mAdapter.notifyDataSetChanged();
    }

    /**
     * Adapter used to show populate the shopping cart with items.
     */
    public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {

        public class ViewHolder extends RecyclerView.ViewHolder {
            public Context mContext;
            public ImageView mItemThumbnailView;
            public TextView mItemNameView;
            public Button mQuantityView;
            public Button mMinusOneButton;
            public Button mPlusOneButton;
            public TextView mTotalPriceView;
            public ViewHolder(View v) {
                super(v);
                mContext = v.getContext();
                mItemThumbnailView = v.findViewById(R.id.thumbnail);
                mItemNameView = v.findViewById(R.id.name);
                mQuantityView = v.findViewById(R.id.quantity);
                mMinusOneButton = v.findViewById(R.id.minusOne);
                mPlusOneButton = v.findViewById(R.id.plusOne);
                mTotalPriceView = v.findViewById(R.id.price);
            }
        }

        /**
         * The list of items to show using this adapter.
         */
        private final List<CartItem> mItems;

        public ItemAdapter() {
            mItems = mCartManager.getItems();
        }

        @Override
        public ItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View cartView = inflater.inflate(R.layout.view_item_cart, parent, false);
            ViewHolder viewHolder = new ViewHolder(cartView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final CartItem item = mItems.get(position);
            holder.mItemThumbnailView.setImageResource(Utilities.getImageIdByName(holder.mContext, item.getThumbnail()));
            holder.mItemNameView.setText(item.getName());
            holder.mQuantityView.setText(Integer.toString(item.getQuantity()));
            holder.mMinusOneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    item.minusOne();
                    holder.mQuantityView.setText(Integer.toString(item.getQuantity()));
                    holder.mTotalPriceView.setText(NumberFormat.getCurrencyInstance().format(item.getTotalPrice()));
                    mCartManager.cleanupItems();
                    update();
                }
            });
            holder.mPlusOneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    item.plusOne();
                    holder.mQuantityView.setText(Integer.toString(item.getQuantity()));
                    holder.mTotalPriceView.setText(NumberFormat.getCurrencyInstance().format(item.getTotalPrice()));
                    mCartManager.cleanupItems();
                    update();
                }
            });

            holder.mTotalPriceView.setText(NumberFormat.getCurrencyInstance().format(item.getTotalPrice()));
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }
    }

}
