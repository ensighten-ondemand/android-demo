package com.ensighten.demo.ui.utils

import android.app.Activity
import androidx.appcompat.app.AlertDialog
import com.ensighten.demo.R
import com.ensighten.demo.model.Item
import com.ensighten.demo.ui.BaseActivity
import com.ensighten.demo.cart.CartManager
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import android.net.Uri
import androidx.core.content.ContextCompat.startActivity




/**
 * Class that contains UI utility functions, such as showing the dialog to add an item to the cart.
 */
class UIUtils {

    interface CartDialogListener {
        fun goToCart();
        fun continueShopping();
    }

    companion object {

        /**
         * Add the specified product to the cart and show the dialog with the message that the item was
         * item to the cart.
         *
         * @param activity: the activity to show the dialog
         * @param product: the product to add to the cart
         */
        fun addToCart(activity: BaseActivity, item: Item, listener: CartDialogListener) {
            CartManager.getInstance(activity).addToCart(item)
            AlertDialog.Builder(activity)
                    .setTitle(activity.getString(R.string.success))
                    .setMessage(String.format(activity.getString(R.string.item_added_to_cart_template), item.name))
                    .setPositiveButton(activity.getString(R.string.go_to_cart)) { dialog, _ ->
                        dialog.dismiss()
                        listener.goToCart()
                    }.setNegativeButton(activity.getString(R.string.continue_shopping)) { dialog, _ ->
                dialog.dismiss()
                listener.continueShopping();
            }.create().show()
        }

        /**
         * Show the dialog to test the deep link tracking functionality.
         *
         * @param activity: the activity to show the dialog
         */
        fun showTestDeepLinkTrackingDialog(activity: Activity) {
            AlertDialog.Builder(activity)
                    .setTitle(activity.getString(R.string.tools_test_deep_link_tracking_dialog_title))
                    .setMessage(activity.getString(R.string.tools_test_deep_link_tracking_dialog_message))
                    .setPositiveButton(activity.getString(android.R.string.ok)) { dialog, _ ->
                        val testDeepLinkIntent = Intent(Intent.ACTION_VIEW)
                        testDeepLinkIntent.data = Uri.parse(activity.getString(R.string.url_ensighten_demo_app))
                        activity.startActivity(testDeepLinkIntent)
                        dialog.dismiss()
                    }.setNegativeButton(activity.getString(android.R.string.cancel)) { dialog, _ ->
                        dialog.dismiss()
                    }.create().show()
        }

        /**
        * Show the dialog to test the app crash tracking functionality.
        *
        * @param activity: the activity to show the dialog
        */
        fun showAppCrashTrackingDialog(activity: Activity) {
            AlertDialog.Builder(activity)
                    .setTitle(activity.getString(R.string.tools_test_app_crash_tracking_dialog_title))
                    .setMessage(activity.getString(R.string.tools_test_app_crash_tracking_dialog_message))
                    .setPositiveButton(activity.getString(R.string.crash)) { dialog, _ ->
                        throw NullPointerException()
                    }.setNegativeButton(activity.getString(android.R.string.cancel)) { dialog, _ ->
                        dialog.dismiss()
                    }.create().show()
        }
    }
}



