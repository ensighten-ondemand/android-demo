package com.ensighten.demo.model

import android.content.Context
import com.ensighten.demo.R
import com.google.gson.annotations.Expose

/**
 * Class representing a store item (product or promotion.
 */
open class Item(id: String, name: String,  thumbnail: String, price: Float) {

    /**
     * The item id.
     */
    @Expose
    val id = id

    /**
     * The item name.
     */
    @Expose
    val name = name

    /**
     * The product thumbnail.
     */
    val thumbnail = thumbnail

    /**
     * The product price.
     */
    @Expose
    val price = price

}