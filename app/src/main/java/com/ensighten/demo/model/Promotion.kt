package com.ensighten.demo.model


/**
 * Class representing a store promotion.
 */
class Promotion (id: String, name: String, thumbnail: String, image: String, price: Float) : Item(id, name, thumbnail, price) {

    /**
     * The promotion image.
     */
    val image = image

}