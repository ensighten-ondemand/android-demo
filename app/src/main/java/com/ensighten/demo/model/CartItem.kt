package com.ensighten.demo.model

import android.content.Context
import com.ensighten.Ensighten
import com.ensighten.demo.R
import com.ensighten.demo.constants.Constants
import com.google.gson.annotations.Expose
import java.text.NumberFormat
import java.util.HashMap

/**
 * Class representing an item inside of the shopping cart
 */
class CartItem(id: String, name: String, thumbnail: String, price: Float) : Item(id, name, thumbnail, price) {

    /**
     * The item quantity.
     */
    @Expose
    var quantity = 1
        set(value) {
            field = value
            totalPrice = quantity * price
        }

    /**
     * The total price.
     */
    var totalPrice = quantity * price

    /**
     * Decrement the quantity by one.
     */
    fun minusOne() {
        if (quantity > 0)
            --quantity
        trackItemQuantityChangedEvent()
    }

    /**
     * Increment the quantity by one.
     */
    fun plusOne() {
        ++quantity;
        trackItemQuantityChangedEvent()
    }

    fun trackItemQuantityChangedEvent() {
        val data = HashMap<String, String>()
        data.put(Constants.JSON_ITEM_ID, id)
        data.put(Constants.JSON_ITEM_NAME, name)
        data.put(Constants.JSON_ITEM_PRICE, NumberFormat.getCurrencyInstance().format(price.toDouble()))
        data.put(Constants.JSON_ITEM_QUANTITY, quantity.toString())
        Ensighten.trackEvent(Constants.ITEM_QUANTITY_CHANGED_EVENT, data)
    }

}