package com.ensighten.demo.model

/**
 * Class representing a store promotion.
 */
class Product(id: String, name: String, description: String, thumbnail: String, images: List<String>, price: Float) : Item(id, name, thumbnail, price) {

    /**
     * The product description.
     */
    val description = description

    /**
     * The product images.
     */
    val images = images

}