package com.ensighten.demo.model;

import java.util.List;

/**
 * Class representing a product in the store.
 */
public class Store {

    /**
     * The list of products available in the store.
     */
    private List<Product> mProducts;

    /**
     * The list of promotions available in the store.
     */
    private List<Promotion> mPromotions;

    /**
     * Create a new store with the specified products and promotions.
     *
     * @param products   the list of products available in the store
     * @param promotions the list of promotions available in the store
     */
    public Store(List<Product> products, List<Promotion> promotions) {
        mProducts = products;
        mPromotions = promotions;
    }

    /**
     * Return the product with the specified id.
     *
     * @param id the id of the product to return
     * @return the product with the specified id
     */
    public Product getProductById(String id) {
        if (id == null)
            return null;

        for (Product product : mProducts) {
            if (id.equals(product.getId())) {
                return product;
            }
        }

        return null;
    }

    /**
     * Return the promotion with the specified id.
     *
     * @param id the id of the promotion to return
     * @return the promotion with the specified id
     */
    public Promotion getPromotionById(String id) {
        if (id == null)
            return null;

        for (Promotion promotion : mPromotions) {
            if (id.equals(promotion.getId())) {
                return promotion;
            }
        }

        return null;
    }

    /**
     * Return the item with the specified id.
     *
     * @param id the id of the item to return
     * @return the item with the specified id
     */
    public Item getItemById(String id) {
        Item item = getProductById(id);

        if (item == null)
            item = getPromotionById(id);

        return item;
    }

    public List<Product> getProducts() {
        return mProducts;
    }

    public void setProducts(List<Product> products) {
        this.mProducts = products;
    }

    public List<Promotion> getPromotions() {
        return mPromotions;
    }

    public void setPromotions(List<Promotion> promotions) {
        this.mPromotions = promotions;
    }
}
