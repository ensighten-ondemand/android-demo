package com.ensighten.demo.tracker;

import com.ensighten.demo.constants.Constants;
import com.ensighten.demo.model.CartItem;
import com.ensighten.demo.model.Item;
import com.ensighten.tracker.Tracker;

import java.util.List;

/**
 * Class containing custom tracker methods.
 */
public class DemoTracker extends Tracker {

    /**
     * Track a checkout event.
     */
    public void trackCheckout(String total, int itemCount, List<CartItem> items) {
        trackCustomMethod(Constants.CHECKOUT_CUSTOM_METHOD, total, itemCount, items);
    }
}
