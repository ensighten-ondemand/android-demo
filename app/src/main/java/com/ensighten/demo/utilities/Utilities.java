package com.ensighten.demo.utilities;

import android.app.Activity;
import android.content.Context;
import android.preference.PreferenceManager;

import com.ensighten.Ensighten;
import com.ensighten.demo.R;
import com.ensighten.demo.constants.Constants;
import com.ensighten.demo.ui.privacy.custom.java.CustomPrivacyBanner;
import com.ensighten.demo.ui.privacy.custom.java.CustomPrivacyModal;
import com.ensighten.demo.ui.privacy.uimanager.DarkThemePrivacyUIManager;
import com.ensighten.demo.ui.privacy.uimanager.LargeFontPrivacyUIManager;
import com.ensighten.privacy.PrivacyUIManager;
import com.google.android.material.snackbar.Snackbar;

/**
 * Class containing set of utility functions used by the app.
 */
public class Utilities {

    /**
     * Get the specified image resource id from the image name.
     *
     * @param context the context of the application
     * @param imageName the name of the image for which to get the resource id
     * @return the image resource id
     */
    public static int getImageIdByName (Context context, String imageName) {
        String packageName = context.getPackageName();
        return context.getResources().getIdentifier(imageName, Constants.RESOURCE_DRAWABLE, packageName);
    }

    /**
     * Update the privacy theme based on the user preferences.
     *
     * @param context the context used to access the user preferences.
     */
    public static void updatePrivacyTheme(Context context) {
        updatePrivacyTheme(context, PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.privacy_theme_key), context.getString(R.string.privacy_theme_custom_key)));
    }

    /**
     * Update the privacy theme based on the user preferences.
     *
     * @param context the context used to access the user preferences.
     * @param theme the theme to update to.
     */
    public static void updatePrivacyTheme(Context context, String theme) {
        Ensighten.getPrivacyManager().setCustomBanner(null);
        Ensighten.getPrivacyManager().setCustomModal(null);
        if (theme.equals(context.getString(R.string.privacy_theme_dark_key))) {
            Ensighten.getPrivacyManager().setPrivacyUIManager(new DarkThemePrivacyUIManager(context));
        } else if (theme.equals(context.getString(R.string.privacy_theme_large_key))) {
            Ensighten.getPrivacyManager().setPrivacyUIManager(new LargeFontPrivacyUIManager());
        } else if (theme.equals(context.getString(R.string.privacy_theme_default_key))) {
            Ensighten.getPrivacyManager().setPrivacyUIManager(new PrivacyUIManager() {});
        } else {
            Ensighten.getPrivacyManager().setCustomBanner(new CustomPrivacyBanner());
            Ensighten.getPrivacyManager().setCustomModal(new CustomPrivacyModal());
        }
    }
}
